const express = require('express')
const routerPerson = require('./routes/student')

const app = express();

app.use(express.json())
//add the router
app.use('/student', routerPerson)

app.listen(4000, '0.0.0.0', () => {
    console.log('server started successfully on port 4000')
})