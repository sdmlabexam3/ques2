const mysql = require('mysql2')

const pool = mysql.createPool({
    host: "localhost",
    user: 'root',
    password: 'manager',
    waitForConnections: true,
    connectionLimit: 20,
    database: 'labexam',
})

module.exports = {
    pool,
}