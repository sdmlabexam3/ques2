const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/',(request, response) => {
 const query = `select * from student`
 db.pool.execute(query, (error, students) =>{
     response.send(utils.createResult(error, students))
 })
})

router.post('/',(request, response) => {
    const { student_id, s_name, password, course ,passing_year, prn_no, dob} = request.body
    
    const query = `insert into student( student_id, s_name, password, course ,passing_year, prn_no, dob) values (?,?,?,?,?,?,?) `
    db.pool.execute(query, [ student_id, s_name, password, course ,passing_year, prn_no, dob ],(error, result) =>{
        response.send(utils.createResult(error, result))
    })
})

router.delete('/:year',(request, response) => {
    const { year } = request.params;
    const query = `delete from student where passing_year = ? `
    db.pool.execute(query, [  year ],(error, result) =>{
        response.send(utils.createResult(error, result))
    })
})

router.put('/:id',(request, response) => {
    const { id } = request.params;
    const { course , prn_no } = request.body
    const query = `update student set course=?, prn_no=? where student_id =?;`
    db.pool.execute(query, [ course,prn_no,id],(error, result) =>{
        response.send(utils.createResult(error, result))
    })
})



module.exports = router 